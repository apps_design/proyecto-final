const fetch = require('node-fetch');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
let pendientes = [];

function fetchData() {
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(res => res.json())
        .then(todos => {
            pendientes = todos;
            menu(pendientes);
        })
        .catch(error => console.log(error));
}

function mostrarpendientesid(pendientes){
    console.log('Lista de todos los pendientes (ID):');
    pendientes.forEach(pendiente => console.log('Id:',pendiente.id));
}
function mostrarpendientesidytitulo(pendientes) {
    console.log('Lista de todos los pendientes (ID y Titulo):');
    pendientes.forEach(pendiente => console.log('Id:', pendiente.id,'- Titulo: ' ,pendiente.title));
}
function mostrarpendientessinresolveridytitulo(pendientes) {
    console.log('Lista de todos los pendientes sin resolver (ID y Titulo):');
    pendientes.forEach(pendiente => {
        if (!pendiente.completed) {
            console.log('Id:', pendiente.id,'- Titulo: ' ,pendiente.title);
}
});
}
function mostrarpendientesresueltosidytitulo(pendientes) {
    console.log('Lista de todos los pendientes resueltos (ID y Titulo):');
    pendientes.forEach(pendiente => {
        if (pendiente.completed) {
            console.log('Id:', pendiente.id,'- Titulo: ' ,pendiente.title);
        }
    });
}
function mostrartodospendientes(pendientes) {
    console.log('Lista de todos los pendientes:');
    pendientes.forEach(pendiente => console.log('Id:', pendiente.id, '- Usuario:'+pendiente.userId));
}
function mostrarpendientesresueltosidyidusuario(pendientes) {
    console.log('Lista de todos los pendientes resueltos (ID y Titulo):');
    pendientes.forEach(pendiente => {
        if (pendiente.completed) {
            console.log('Id:', pendiente.id, '- Usuario:',pendiente.userId);
        }
    });
}
function mostrarpendientessinresolveridyusurio(pendientes) {
    console.log('Lista de todos los pendientes sin resolver (ID y Usuario):');
    pendientes.forEach(pendiente => {
        if (!pendiente.completed) {
            console.log('Id:', pendiente.id, '- Usuario:', pendiente.userId);
        }
    });
}
function menu() {
    console.log("\nSistema de pendientes de la NFL")
    console.log('\nMenu\n');
    console.log('1. Mostrar todos los pendientes (ID)');
    console.log('2. Mostrar todos los pendientes (ID y Titulo)');
    console.log('3. Mostrar todos los pendientes sin resolver (ID Y Titulo)');
    console.log('4. Mostrar todos los pendientes resueltos (ID y Titulo)');
    console.log('5. Mostrar todos los pendientes (ID y Usuario)');
    console.log('6. Mostrar todos los pendientes resueltos (ID y Usuario)');
    console.log('7. Mostrar todos los pendientes sin resolver (ID y Usuario)');
    console.log('8. Salir del sistema');
    rl.question('Escriba la opcion que requiere consultar:',(input)=>{
        switch (input) {
            case '1':
                mostrarpendientesid(pendientes);
                menu();
                break;
            case '2':
                mostrarpendientesidytitulo(pendientes);
                menu();
                break;
            case '3':
                mostrarpendientessinresolveridytitulo(pendientes);
                menu();
                break;
            case '4':
                mostrarpendientesresueltosidytitulo(pendientes);
                menu();
                break;
            case '5':
                mostrartodospendientes(pendientes);
                menu();
                break;
            case '6':
                mostrarpendientesresueltosidyidusuario(pendientes);
                menu();
                break;
            case '7':
                mostrarpendientessinresolveridyusurio(pendientes);
                menu();
                break;
            case '8':
                console.log('Sistema apagado');
                rl.close();
                break;
            default:
                console.log('Opcion no valida');
                menu();
                break;
        }
    });
}
fetchData();